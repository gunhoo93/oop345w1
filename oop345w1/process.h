// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-09-07
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#ifndef PROCESS_H
#define PROCESS_H

#include <iostream>
#include "CString.h"

void process(const char*, std::ostream&);

#endif // PROCESS_H