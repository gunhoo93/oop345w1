// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-09-07
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#include <cstring>
#include <iostream>

#include "CString.h"

using namespace std;

size_t STORED = 3; // number of characters stored for CString without null terminator

namespace w1 {

	char* copy_cstr(char*, const char*, size_t);

	CString::CString(const char* cstr)
		: m_cstr{ nullptr }, MAX { STORED }
	{
		char *tmp = new char[MAX + 1];
		copy_cstr(tmp, cstr, MAX);
		m_cstr = tmp;
	}

	// dest size must be at least length + 1
	// Otherwise, it might read from illegal memory.
	char* copy_cstr(char* dest, const char* src, size_t length) {
		strncpy(dest, src, length);
		dest[length] = '\0';

		return dest;
	}

	CString::~CString() {
		delete[] m_cstr;
	}

	void CString::display(ostream& ost) const {
		ost << m_cstr;
	}

	ostream& operator<<(ostream& ost, const CString& obj) {
		static long long insert_count = 0;
		ost << insert_count << ": ";
		obj.display(ost);

		return ost;
	}

}