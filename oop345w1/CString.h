// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-09-07
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#ifndef W1_CSTRING_H
#define W1_CSTRING_H

#include <iostream>

namespace w1 {	
	class CString {
	public:
		CString(const char* cstr);
		~CString();

		CString(const CString&) = delete;
		CString(CString&&) = delete;

		void display(std::ostream&) const;

	private:
		const char* m_cstr;
		const size_t MAX;
	};

	std::ostream& operator<<(std::ostream&, const CString&);

}

#endif // W1_CSTRING_H