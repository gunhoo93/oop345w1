// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-09-07
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#include "process.h"

#include <iostream>
#include "CString.h"

using namespace std;
using namespace w1;

void process(const char* cstr, ostream& ost) {
	cout << CString{ cstr } << endl;
}